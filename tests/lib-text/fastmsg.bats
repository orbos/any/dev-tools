#!/usr/bin/env bats

setup() {
  . "$HOME"/gitlab/dev-tools/lib/lib-text
  msgspd fast
  commaspeed="0"
  periodspeed="0"
  speed="0"
  linespeed="0"
}
@test "fastmsg - msg accuracy test." {
 run msg "normal text, test."
 [ "$output" = "normal text, test." ]
}
teardown() {
  cleanup_libtext
  cleanup_libtextconf
}

setup() {
  . "$HOME"/gitlab/dev-tools/lib/lib-text
  msgspd fast
  commaspeed="0"
  periodspeed="0"
  speed="0"
  linespeed="0"
  ManArray=$( man wc )
}
@test "fastmsg - man page output msg accuracy test." {
 run msg "${ManArray[@]}"
 [ "$output" = "${ManArray[@]}" ]
}
teardown() {
  cleanup_libtext
  cleanup_libtextconf
}

setup() {
  . "$HOME"/gitlab/dev-tools/lib/lib-text
  msgspd fast
  commaspeed="0"
  periodspeed="0"
  speed="0"
  linespeed="0"
}
@test "fastmsg - qmsg accuracy test." {
 run qmsg "normal text, test."
 [ "$output" = "normal text, test." ]
}
teardown() {
  cleanup_libtext
}

setup() {
  . "$HOME"/gitlab/dev-tools/lib/lib-text
  msgspd fast
  commaspeed="0"
  periodspeed="0"
  speed="0"
  linespeed="0"
}
@test "fastmsg - rmsg accuracy test." {
  skip
 run rmsg "normal text, test."
 [ "$output" = "normal text, test." ]
}
teardown() {
  cleanup_libtext
  cleanup_libtextconf
}

setup() {
  . "$HOME"/gitlab/dev-tools/lib/lib-text
  msgspd fast
  commaspeed="0"
  periodspeed="0"
  speed="0"
  linespeed="0"
}
@test "fastmsg - msgr accuracy test." {
 run msgr "normal text, test."
 [ "$output" = "normal text, test." ]
}
teardown() {
  cleanup_libtext
  cleanup_libtextconf
}

setup() {
  . "$HOME"/gitlab/dev-tools/lib/lib-text
  msgspd fast
  commaspeed="0"
  periodspeed="0"
  speed="0"
  linespeed="0"
}
@test "fastmsg - rmsgr accuracy test." {
 run rmsgr "normal text, test."
 [ "$output" = "normal text, test." ]
}
teardown() {
  cleanup_libtext
  cleanup_libtextconf
}

setup() {
  . "$HOME"/gitlab/dev-tools/lib/lib-text
  msgspd fast
  commaspeed="0"
  periodspeed="0"
  speed="0"
  linespeed="0"
}
@test "fastmsg - qmsgr accuracy test." {
 run qmsgr "normal text, test."
 [ "$output" = "normal text, test." ]
}
teardown() {
  cleanup_libtext
  cleanup_libtextconf
}

setup() {
  . "$HOME"/gitlab/dev-tools/lib/lib-text
  msgspd fast
  commaspeed="0"
  periodspeed="0"
  speed="0"
  linespeed="0"
}
@test "fastmsg - rqmsg accuracy test." {
 run rqmsg "normal text, test."
 [ "$output" = "normal text, test." ]
}
teardown() {
  cleanup_libtext
  cleanup_libtextconf
}

setup() {
  . "$HOME"/gitlab/dev-tools/lib/lib-text
  msgspd fast
  commaspeed="0"
  periodspeed="0"
  speed="0"
  linespeed="0"
}
@test "fastmsg - rqmsgr accuracy test." {
 run rqmsgr "normal text, test."
 [ "$output" = "normal text, test." ]
}
teardown() {
  cleanup_libtext
  cleanup_libtextconf
}