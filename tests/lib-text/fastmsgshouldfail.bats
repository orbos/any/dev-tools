#!/usr/bin/env bats

@test "fastmsg - man page output msg accuracy test - Should fail." {
 run fastmsgshouldfail.bash
 [ "$status" -eq 0 ]
}