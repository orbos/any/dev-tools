#!/usr/bin/env bash

. "$HOME"/gitlab/dev-tools/lib/lib-text

msgspd fast
commaspeed="0"
periodspeed="0"
speed="0"
linespeed="0"
ManArray=$( man wc )
OutputArray=$( msg "${ManArray[@]}" )

if [[ "${ManArray[@]}" = "${OutputArray[@]}" ]]
then
  exit "0"
else
  exit "1"
fi

